# FSC Istio Sidecar
This is an experiment to see if we can adopt FSC without making changes to our (micro)services. It leverages an 
Istio sidecar for this.

If you don't like reading, just run the experiment:

```bash
asdf install
make dev
```

It will create a k3d cluster and deploy the test services and an FSC outway mock instance.
When all is running, [visit the consumer](http://consumer.127-0-0-1.nip.io:8080), which will make a request to both provider APIs.

## Situation without FSC (simplified)
Consumer service A can make direct requests to provider API A and provider API B.
![Situation before FSC](assets/situation-no-fsc.png)

The requests consists of a destination host (e.g. `api-a` or `api-b`) and a path (e.g. `/api/v1/endpoint`). It may 
include some form of authentication (e.g. a JWT token), but that is not relevant for this experiment.

## Situation with FSC (simplified)
If we want to use FSC in this setup, typically two changes need to be made:

- the destination of the requests needs to be changed to the outway of the organisation
- a header needs to be added to every request, containing the corresponding grant hash of the contract between the 
  consumer and the provider.

The situation with FSC would look like this:

![Situation with FSC](assets/situation-with-fsc.png)

Every request consumer A makes will go to FSC, and FSC will route the request to the correct provider API based on 
the grant hash provided in the header.

## Goal of this experiment
The requirement to make these two changes to your services have two downsides:

- it costs extra work/time to adopt FSC
- it couples the services to FSC

That's why we want to see if it is possible to add an Istio sidecar to every consumer service, that replaces the 
destination host to the organisation's outway address, add injects the right FSC Grant Hash in the headers. It 
should use the hash that corresponds with the origin and the original destination (e.g. consumer A to provider B).

To make this test easy, we faked an FSC outway that simply acts as a proxy and select the right backend base on the 
grant hash. However, this exact same approach will work if you switch it for the real stuff.

## Situation with FSC and istio
We install istio in our cluster, and configure istio to inject a sidecar into every consumer pod. Every request a 
consumer make, will go through this sidecar (Envoy proxy). This sidecar will:

- replace the destination host with the outway address using an Istio [VirtualService](https://istio.io/latest/docs/reference/config/networking/virtual-service/), see [this manifest](./deploy/kustomize/consumer/virtual-service.yaml).
- inject the correct FSC Grant Hash as a header in the request using an Istio [EnvoyFilter](https://istio.io/latest/docs/reference/config/networking/envoy-filter/), see [this manifest](./deploy/kustomize/consumer/envoy-filter.yaml)

![Situation with FSC](assets/situation-proxy.png)

## Results
The experiment worked. Consumer A used to make a request like this:

```python
import requests
requests.get('http://api-a/api/v1/endpoint')
```

And this still works, but now through FSC. And the best part is that we were able to use istio and didn't need to 
write any code ourselves; this should make the adoption easier because istio is the standard for service meshes.
