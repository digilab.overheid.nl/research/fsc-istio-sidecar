from flask import Flask, jsonify, request

app = Flask(__name__)

@app.route("/")
def index():
    d = {"data": "Here is some data from provider B"}
    return jsonify(d)
