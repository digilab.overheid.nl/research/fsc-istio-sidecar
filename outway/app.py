from flask import Flask, jsonify, request
import requests

app = Flask(__name__)


@app.route("/")
def index():
    headers = {
        "Accept": "application/json",
    }

    data = {
        # "headers": dict(request.headers),
        "fsc_grant_hash": request.headers.get('Fsc-Grant-Hash'),
    }

    fsc_grant_hash = request.headers.get('Fsc-Grant-Hash')

    if fsc_grant_hash == "consumer-to-provider-a-hash":
        data["api"] = requests.get("http://provider-a-svc-direct.fsc-istio.svc.cluster.local", headers=headers).json()
    elif fsc_grant_hash == "consumer-to-provider-b-hash":
        data["api"] = requests.get("http://provider-b-svc-direct.fsc-istio.svc.cluster.local", headers=headers).json()
    else:
        data["message"] = f"Invalid Fsc-Grant-Hash: {fsc_grant_hash}"

    return jsonify(data)
