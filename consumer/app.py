from flask import Flask, jsonify
import requests

app = Flask(__name__)


@app.route("/")
def index():
    headers = {
        "Accept": "application/json",
    }
    # res = requests.get("http://neverssl.com", headers=headers)
    api_a = requests.get("http://provider-a-svc.fsc-istio.svc.cluster.local", headers=headers)
    api_b = requests.get("http://provider-b-svc.fsc-istio.svc.cluster.local", headers=headers)

    return jsonify({
        "api-a": api_a.json(),
        "api-b": api_b.json(),
    })
