PROJECT_NAME := fsc-istio
CLUSTER_EXISTS := $$(k3d cluster list $(PROJECT_NAME) --no-headers | wc -l | xargs)
ROOT_DIR := ${PWD}

.PHONY: k3d
k3d:
	@if [ $(CLUSTER_EXISTS) -eq 0 ]; then \
		KUBECONFIG= k3d cluster create --config=deploy/k3d/config.yaml; \
	else \
		k3d cluster start "$(PROJECT_NAME)"; \
	fi
	kubectl config use-context "k3d-$(PROJECT_NAME)"

.PHONY: dev
dev: k3d
	skaffold dev --cleanup=false

.PHONY: stop
stop:
	k3d cluster stop "$(PROJECT_NAME)"

.PHONY: clean
clean:
	k3d cluster delete "$(PROJECT_NAME)"
